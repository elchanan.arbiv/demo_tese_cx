package com.example.demo.student;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
//user222 to master from line 5

//fflfffdddghjkm,ff
import java.time.LocalDate;
import java.time.Month;
import java.util.List;
@Service
public class StudentService {

    public List<Student> getStudent() {
        return List.of(
                new Student(
                        1L,
                        "user11-pull-request11",
                        "master3.@gmail.com",
                        LocalDate.of(2000, Month.APRIL, 4),
                        23
                )
        );
    }
}
